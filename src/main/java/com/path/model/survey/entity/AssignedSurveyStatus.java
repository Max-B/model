package com.path.model.survey.entity;

public enum AssignedSurveyStatus {

    ASSIGNED,
    PARTLY_COMPLETED,
    COMPLETED,
    OUTDATED
}
