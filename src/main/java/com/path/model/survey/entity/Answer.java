package com.path.model.survey.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "answer")
@Entity
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;

    String description;

    int number;

    int score;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question.id", nullable = false)
    @JsonIgnore
    Question question;
}
