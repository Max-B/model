package com.path.model.survey.repository;

import com.path.model.survey.entity.AssignedSurvey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssignedSurveyRepository extends JpaRepository<AssignedSurvey, Long>{

    List<AssignedSurvey> findAllByPatientName(String patientName);
}
