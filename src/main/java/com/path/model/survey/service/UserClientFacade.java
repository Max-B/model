package com.path.model.survey.service;

import com.path.client.api.DefaultApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserClientFacade {

    @Autowired
    private DefaultApi defaultApi;

    public String getCurrentUserName() {
        return defaultApi.getCurrentUserName();
    }
}
