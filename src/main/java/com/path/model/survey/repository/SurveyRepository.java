package com.path.model.survey.repository;

import com.path.model.survey.entity.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SurveyRepository extends JpaRepository<Survey, Long> {

    Survey findByName(String name);
}
