package com.path.model.survey.service;

import com.path.model.survey.dto.SurveyDto;
import com.path.model.survey.dto.SurveyResultsDto;
import com.path.model.survey.entity.AssignedSurvey;

import java.util.List;

public interface SurveyService {



    Long addSurvey(SurveyDto survey);

    List<SurveyDto> findAll();

    long assignSurvey(String currentUserName, String patientName, String survey);

    void submitSurveyResult(String patientName, Long assignedSurveyId, Long questionId, SurveyResultsDto surveyResultsDto);

    List<AssignedSurvey> getAssignedSurveysForPatient(String patient);
}
