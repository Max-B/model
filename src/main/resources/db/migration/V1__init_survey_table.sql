CREATE TABLE survey
(
    id bigint NOT NULL,
    name character varying(255),
    description character varying(500),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE SEQUENCE survey.hibernate_sequence
   INCREMENT 1
   START 1
   MINVALUE 1
   MAXVALUE 9223372036854775807
   CACHE 1;