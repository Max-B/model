package com.path.model.survey.config;

import com.path.client.api.DefaultApi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

    @Bean
    public DefaultApi defaultApi() {
        DefaultApi defaultApi = new DefaultApi();
        defaultApi.getApiClient().setBasePath("http://localhost:8081");
        defaultApi.getApiClient().addDefaultHeader("Authorization", "Basic ZGV2OmRldmls");
        return defaultApi;
    }
}
