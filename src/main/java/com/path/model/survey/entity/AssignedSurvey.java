package com.path.model.survey.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
public class AssignedSurvey {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;

    String patientName;

    String therapistName;

    @Enumerated(EnumType.STRING)
    AssignedSurveyStatus status;

    @ManyToOne
    @JoinColumn(name = "survey.id", nullable = false)
    Survey survey;

    LocalDate assignmentDate;

    LocalDate dueDate;

    @OneToMany(mappedBy = "assignedSurvey", cascade = CascadeType.ALL)
    List<AssignedSurveyResults> results;

}