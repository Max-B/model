package com.path.model.survey.repository;

import com.path.model.survey.entity.AssignedSurveyResults;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SurveyResultsRepository extends JpaRepository<AssignedSurveyResults, Long> {
}
