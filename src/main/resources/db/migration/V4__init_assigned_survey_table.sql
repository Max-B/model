CREATE TABLE assigned_survey
(
    id bigint NOT NULL,
    survey_id bigint NOT NULL,
    therapist_name character varying(255),
    patient_name character varying(255),
    status character varying(255),
    assignment_date date,
    due_date date,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE TABLE assigned_survey_results
(
    id bigint NOT NULL,
    assigned_survey_id bigint NOT NULL,
    question_number int NOT NULL,
    answer_number int NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
