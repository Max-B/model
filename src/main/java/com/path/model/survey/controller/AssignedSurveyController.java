package com.path.model.survey.controller;

import com.path.model.survey.dto.SurveyResultsDto;
import com.path.model.survey.entity.AssignedSurvey;
import com.path.model.survey.service.SurveyService;
import com.path.model.survey.service.UserClientFacade;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/assigned-survey")
@AllArgsConstructor
public class AssignedSurveyController {

    private final UserClientFacade userClientFacade;
    private final SurveyService surveyService;

    @GetMapping(value = "/{patientName}",produces = "application/json")
    public List<AssignedSurvey> surveyResult(@PathVariable String patientName) {
        String patient = Objects.isNull(patientName) ? userClientFacade.getCurrentUserName() : patientName;
        return surveyService.getAssignedSurveysForPatient(patient);
    }

    @PostMapping(value = "/{name}/assignTo/{patientName}",produces = "application/json")
    public long assignSurvey(@PathVariable String name, @PathVariable String patientName) {
        String theraphystName = userClientFacade.getCurrentUserName();
        return surveyService.assignSurvey(theraphystName, patientName, name);
    }


    @PostMapping(value = "/{assignedSurveyId}/question/{questionNumber}",produces = "application/json")
    public void surveyResult(@PathVariable Long assignedSurveyId,
                             @PathVariable Long questionNumber,
                             @RequestBody SurveyResultsDto surveyResultsDto) {
        String patientName = userClientFacade.getCurrentUserName();
        surveyService.submitSurveyResult(patientName, assignedSurveyId, questionNumber, surveyResultsDto);
    }
}
