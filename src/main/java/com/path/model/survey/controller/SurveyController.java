package com.path.model.survey.controller;

import com.path.model.survey.dto.SurveyDto;
import com.path.model.survey.service.SurveyService;
import com.path.model.survey.service.UserClientFacade;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/survey")
@AllArgsConstructor
public class SurveyController {

    private final SurveyService surveyService;

    @PostMapping(produces = "application/json")
    public long addSurvey(@RequestBody SurveyDto survey) {
        return surveyService.addSurvey(survey);
    }

    @GetMapping(produces = "application/json")
    public List<SurveyDto> getSurveyByName() {
        return surveyService.findAll();
    }

}
