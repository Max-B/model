package com.path.model.survey.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "survey")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    String name;

    String description;

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
    List<Question> questions;

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
    @JsonIgnore
    List<AssignedSurvey> assignedSurveys;
}
