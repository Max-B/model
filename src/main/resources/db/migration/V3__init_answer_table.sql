CREATE TABLE answer
(
    id bigint NOT NULL,
    question_id bigint NOT NULL,
    description character varying(500),
    number int,
    score int,
    PRIMARY KEY (id),

    FOREIGN KEY (question_id) REFERENCES question (id)
)
WITH (
    OIDS = FALSE
);

