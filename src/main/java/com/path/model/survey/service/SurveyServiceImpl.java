package com.path.model.survey.service;

import com.path.model.survey.dto.SurveyDto;
import com.path.model.survey.dto.SurveyResultsDto;
import com.path.model.survey.entity.AssignedSurvey;
import com.path.model.survey.entity.AssignedSurveyResults;
import com.path.model.survey.entity.AssignedSurveyStatus;
import com.path.model.survey.entity.Survey;
import com.path.model.survey.repository.AssignedSurveyRepository;
import com.path.model.survey.repository.SurveyResultsRepository;
import com.path.model.survey.repository.SurveyRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SurveyServiceImpl implements SurveyService {

    private final SurveyRepository surveyRepository;
    private final AssignedSurveyRepository assignedSurveyRepository;
    private final SurveyResultsRepository surveyResultsRepository;

    private final ModelMapper modelMapper;

    @Override
    public Long addSurvey(SurveyDto survey) {
        Survey surveyEntity = modelMapper.map(survey, Survey.class);
        surveyEntity.getQuestions().forEach(question -> question.setSurvey(surveyEntity));
        surveyEntity.getQuestions().forEach(question -> {
            if (Objects.nonNull(question.getAnswers()))
                question.getAnswers().forEach(answer -> answer.setQuestion(question));
        });
        Survey savedSurvey = surveyRepository.save(surveyEntity);
        return savedSurvey.getId();
    }

    @Override
    public List<SurveyDto> findAll() {
        return surveyRepository.findAll().stream().map(survey -> modelMapper.map(survey, SurveyDto.class)).collect(Collectors.toList());
    }

    @Override
    public long assignSurvey(String currentUserName, String patientName, String survey) {

        AssignedSurvey entity = new AssignedSurvey();
        entity.setTherapistName(currentUserName);
        entity.setPatientName(patientName);
        entity.setSurvey(surveyRepository.findByName(survey));
        entity.setAssignmentDate(LocalDate.now());
        entity.setDueDate(LocalDate.of(2999, 01, 01));
        entity.setStatus(AssignedSurveyStatus.ASSIGNED);
        return assignedSurveyRepository.save(entity).getId();
    }

    @Override
    public void submitSurveyResult(String patientName, Long assignedSurveyId, Long questionNumber, SurveyResultsDto surveyResultsDto) {
        AssignedSurvey assignedSurvey = assignedSurveyRepository.findById(assignedSurveyId)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find such assigned survey by id "));
        if (assignedSurvey.getPatientName().equals(patientName))
            throw new IllegalStateException("Current user doesn't match one to whom survey is assigned");
        surveyResultsDto.getAnswers().forEach(answerNumber -> {
                AssignedSurveyResults assignedSurveyResults = new AssignedSurveyResults();
                assignedSurveyResults.setAssignedSurvey(assignedSurvey);
                assignedSurveyResults.setQuestionNumber(questionNumber);
                assignedSurveyResults.setAnswerNumber(answerNumber);
                surveyResultsRepository.saveAndFlush(assignedSurveyResults);
                assignedSurvey.setStatus(AssignedSurveyStatus.PARTLY_COMPLETED);
                assignedSurveyRepository.saveAndFlush(assignedSurvey);
        });

    }

    @Override
    public List<AssignedSurvey> getAssignedSurveysForPatient(String patientname) {
        return assignedSurveyRepository.findAllByPatientName(patientname);
    }
}
