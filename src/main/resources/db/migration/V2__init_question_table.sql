CREATE TABLE question
(
    id bigint NOT NULL,
    survey_id bigint NOT NULL,
    number bigint,
    description character varying(500),
    multiple_answers_allowed boolean,
    PRIMARY KEY (id),

    FOREIGN KEY (survey_id) REFERENCES survey (id)
)
WITH (
    OIDS = FALSE
);

