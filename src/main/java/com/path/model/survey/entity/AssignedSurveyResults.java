package com.path.model.survey.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class AssignedSurveyResults {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    long id;

    @ManyToOne
    @JoinColumn(name = "assigned_survey.id")
    @JsonIgnore
    AssignedSurvey assignedSurvey;

    long questionNumber;

    long answerNumber;
}
